import mysql.connector
from mysql.connector import errorcode
import flask
from flask import request, jsonify

app = flask.Flask(__name__)
app.config["DEBUG"] = True

class DbConnect(object):

    def __init__(self, db_username, db_password, db_host, dbName):
        self.db_username = db_username
        self.db_password = db_password
        self.db_host = db_host
        self.dbName = dbName

    def get_connection(self):
        try:
            connection = mysql.connector.connect( user=self.db_username, host= self.db_host, database=self.dbName)
            return connection
        except mysql.connector.Error as err:
          if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
          elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
          else:
            print(err)
        else:
          connection.close()



db_conn = DbConnect('root', 'root', '127.0.0.1', 'grofers')

@app.route('/getValue', methods=['GET'])
def get_data():
    if 'keyName' in request.args:
        keyName = request.args['keyName']
        conn=db_conn.get_connection()
        cursor=conn.cursor()
        try:
            sql = "select keyName, keyValue FROM subscribeData where keyName = %s"
            cursor.execute(sql, (keyName,))
            row = cursor.fetchall()
            if len(row) == 0:
                return "please pass correct keyName"
            else:
                for keyName, keyValue in row:
                    return ("Values : " + keyValue)
                cursor.close()
                conn.close()
        except mysql.connector.ProgrammingError as err:
            return ""
    else:
        return "Please pass keyName as params"

@app.route('/update', methods=['PUT'])
def update_data():
    keyName=request.form['keyName']
    keyValue=request.form['keyValue']
    conn=db_conn.get_connection()
    try:
        cursor=conn.cursor()
        sql = "UPDATE subscribeData SET keyValue= %s, updateStatus='Y'  WHERE keyName= %s"
        cursor.execute(sql, (keyValue, keyName))
        conn.commit()
        cursor.close()
        conn.close()
        return ("Data Updated")
    except mysql.connector.ProgrammingError as err:
        return err

@app.route('/subscribe', methods=['POST'])
def subscribe_data():
    keyName=request.form['keyName']
    keyValue=request.form['keyValue']
    conn=db_conn.get_connection()
    try:
        cursor=conn.cursor()
        sql = "INSERT INTO subscribeData (keyName, keyValue) VALUES (%s, %s)"
        cursor.execute(sql, (keyName, keyValue))
        conn.commit()
        cursor.close()
        conn.close()
        return ("Subscribed")
    except mysql.connector.ProgrammingError as err:
        return err

@app.route('/unSubscribe', methods=['DELETE'])
def delete_data():
    keyName=request.form['keyName']
    conn=db_conn.get_connection()
    try:
        cursor=conn.cursor()
        sql = "DELETE FROM subscribeData where keyName = %s"
        cursor.execute(sql, (keyName,))
        conn.commit()
        cursor.close()
        conn.close()
        return ("UnSubscribed")
    except mysql.connector.ProgrammingError as err:
        return err


@app.route('/watch', methods=['GET'])
def watch_data():
        conn=db_conn.get_connection()
        cursor=conn.cursor()
        sql = "select keyName, keyValue FROM subscribeData where updateStatus = 'Y'"
        try:
            cursor.execute(sql)
            row = cursor.fetchall()
            if len(row) == 0:
                return ""
            else:
                keyVal_dic = { }
                for keyName, keyValue in row:
                    sql = "UPDATE subscribeData SET updateStatus='N' WHERE keyName= %s"
                    cursor.execute(sql, (keyName,))
                    conn.commit()
                    keyVal_dic[keyName] = keyValue
                cursor.close()
                conn.close()
                return (keyVal_dic)
        except mysql.connector.ProgrammingError as err:
            return ""
app.run()
