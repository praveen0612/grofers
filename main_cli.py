import mysql.connector
from mysql.connector import errorcode
import sys
import time

class DbConnect(object):

    def __init__(self, db_username, db_password, db_host, dbName):
        self.db_username = db_username
        self.db_password = db_password
        self.db_host = db_host
        self.dbName = dbName

    def get_connection(self):
        try:
            connection = mysql.connector.connect( user=self.db_username, host= self.db_host, database=self.dbName)
            return connection
        except mysql.connector.Error as err:
          if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
          elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
          else:
            print(err)
        else:
          connection.close()

def get_data():
    if len(sys.argv) == 3:
        keyName = sys.argv[2]
        conn=db_conn.get_connection()
        cursor=conn.cursor()
        try:
            sql = "select keyName, keyValue FROM subscribeData where keyName = %s"
            cursor.execute(sql, (keyName,))
            row = cursor.fetchall()
            if len(row) == 0:
                print ("please pass correct keyName")
            else:
                for keyName, keyValue in row:
                    print ("Values : " + keyValue)
                cursor.close()
                conn.close()
        except mysql.connector.ProgrammingError as err:
            print ("")
    else:
        print ("Please pass keyName as params")

def update_data(keyName, keyValue):
    conn=db_conn.get_connection()
    try:
        cursor=conn.cursor()
        sql = "UPDATE subscribeData SET keyValue= %s, updateStatus='Y'  WHERE keyName= %s"
        cursor.execute(sql, (keyValue, keyName))
        conn.commit()
        cursor.close()
        conn.close()
        print ("Data Updated")
    except mysql.connector.ProgrammingError as err:
        print (err)

def subscribe_data(keyName, keyValue):
    conn=db_conn.get_connection()
    try:
        cursor=conn.cursor()
        sql = "INSERT INTO subscribeData (keyName, keyValue) VALUES (%s, %s)"
        cursor.execute(sql, (keyName, keyValue))
        conn.commit()
        cursor.close()
        conn.close()
        print ("Subscribed")
    except mysql.connector.ProgrammingError as err:
        print (err)

def delete_data(keyName):
    conn=db_conn.get_connection()
    try:
        cursor=conn.cursor()
        sql = "DELETE FROM subscribeData where keyName = %s"
        cursor.execute(sql, (keyName,))
        conn.commit()
        cursor.close()
        conn.close()
        print ("UnSubscribed")
    except mysql.connector.ProgrammingError as err:
        print (err)

def watch_data():
    while 1:
        conn=db_conn.get_connection()
        cursor=conn.cursor()
        sql = "select keyName, keyValue FROM subscribeData where updateStatus = 'Y'"
        try:
            cursor.execute(sql)
            row = cursor.fetchall()
            if len(row) == 0:
                time.sleep(1)
                continue
            else:
                keyVal_dic = { }
                for keyName, keyValue in row:
                    sql = "UPDATE subscribeData SET updateStatus='N' WHERE keyName= %s"
                    cursor.execute(sql, (keyName,))
                    conn.commit()
                    keyVal_dic[keyName] = keyValue
                cursor.close()
                conn.close()
                print (keyVal_dic)
                time.sleep(1)

        except mysql.connector.ProgrammingError as err:
            print ("")

if __name__ == "__main__":
    db_conn = DbConnect('root', 'root', '127.0.0.1', 'grofers')
    if len(sys.argv) > 1:
        if sys.argv[1] == 'get':
            get_data()
        elif sys.argv[1] == 'put':
            if len(sys.argv) == 4:
                update_data(sys.argv[2], sys.argv[3])
            else:
                print ("Please enter keyName and keyValue")
        elif sys.argv[1] == 'delete':
            if len(sys.argv) == 3:
                delete_data(sys.argv[2])
            else:
                print ("Please enter keyName")
        elif sys.argv[1] == 'post':
            if len(sys.argv) == 4:
                subscribe_data(sys.argv[2], sys.argv[3])
            else:
                print ("Please enter keyName and keyValue")
        elif sys.argv[1] == 'watch':
            watch_data()
        else:
            print ("Please pass the correct Request")
    else:
        print ("Please pass Request")
