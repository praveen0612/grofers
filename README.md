# To RUN the given Script
1. Run python with flask as a backend service
2. Run on your local terminal

## Pre Requisite
  1. Make sure Python, pip and mysql DB installed in your system.
  2. port 5000 is not used by any other services

## Configure DB and module installation

1. create a DB with name 'grofers'
   create database grofers;

2. create Table with name 'subscribeData'

    CREATE TABLE subscribeData (
    keyName VARCHAR(20) PRIMARY KEY,
    keyValue VARCHAR(30) NOT NULL,
    DateEntered DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    DateUpdated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    updateStatus VARCHAR(5) DEFAULT 'Y');

3. pip3 install -r requirements.txt

## Step 1 RUN AS API:

4. python3 main_api.py

### Below are the list of Curl api to perform the task

1. To subscribe new key in system, you need to pass keyName and keyValue in below request

   curl --location --request POST 'http://127.0.0.1:5000/subscribe' --form 'keyName="<keyName>"' --form 'keyValue="<keyValue>"'

2. To Update the value of given key in system. you need to pass keyName and keyValue in below request

   curl --location --request PUT 'http://127.0.0.1:5000/update' --form 'keyName="<keyName>"' --form 'keyValue="<keyValue>"'

3. To UnSubscribed any data from the system, you need to pass keyName in below request

   curl --location --request DELETE 'http://127.0.0.1:5000/unSubscribe' --form 'keyName="<keyName>"'

4. To Get Value corresponding to any key, you need to pass keyName in below request

   curl --location --request GET 'http://127.0.0.1:5000/getValue?keyName=<keyName>'

5. To Watch the changes in key values, run below command in infinite loop

   while true; do   curl -X GET http://127.0.0.1:5000/watch; sleep 1; done


## Step 2 RUN AS CLI:

### Below are the list of CLI command to perform the task

1. To subscribe new key in system, you need to pass keyName and keyValue in below request

   python3 main_cli.py post <keyName> <keyValue>

2. To Update the value of given key in system. you need to pass keyName and keyValue in below request

   python3 main_cli.py put <keyName> <keyValue>

3. To UnSubscribed any data from the system, you need to pass keyName in below request

   python3 main_cli.py delete <keyName>

4. To Get Value corresponding to any key, you need to pass keyName in below request

   python3 main_cli.py get <keyName>

5. To Watch the changes in key values, run below command in infinite loop

   python3 main_cli.py watch
